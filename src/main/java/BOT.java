import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class BOT extends TelegramLongPollingBot {

    public void onUpdateReceived(Update update) {

        String command  = update.getMessage().getText();
        SendMessage message = new SendMessage();

        switch (command) {
            case "/myname":
                message.setText(update.getMessage().getFrom().getFirstName());
                break;

            case "/mylastname":
                message.setText(update.getMessage().getFrom().getLastName());
                break;

            case "/myfullname":
                message.setText(update.getMessage().getFrom().getFirstName() +" "+ update.getMessage().getFrom().getLastName());
                break;

            default:
                message.setText((update.getMessage().getFrom().getFirstName()) + ", извините, я вас не понимаю.");
        }

        message.setChatId(update.getMessage().getChatId());

        try {
            execute(message);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }

    }

    public String getBotUsername() {
        return "";
    }

    public String getBotToken() {
        return "";
    }
}